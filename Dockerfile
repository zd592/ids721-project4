# Start with Rust as the builder stage
FROM rust:1.68 as builder
WORKDIR /usr/src/myapp

# Copy the project files into the Docker image
COPY . .

# Build the project
RUN cargo build --release

# Start a new stage to create a smaller final image
FROM gcr.io/distroless/cc-debian11
COPY --from=builder /usr/src/myapp/target/release/idsproject4 /usr/local/bin/idsproject4

# Expose the port the application listens on
EXPOSE 8080

# Set the command to run your binary
CMD ["idsproject4"]
